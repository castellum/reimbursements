VIRTUAL_ENV ?= .venv
MANAGEPY = $(VIRTUAL_ENV)/bin/python manage.py

.PHONY: all
all: install migrate populate run

.PHONY: run
run:
	$(MANAGEPY) runserver

.PHONY: install
install:
	if [ ! -d "$(VIRTUAL_ENV)" ]; then python3 -m venv "$(VIRTUAL_ENV)"; fi
	$(VIRTUAL_ENV)/bin/python -m pip install -U pip wheel
	$(VIRTUAL_ENV)/bin/python -m pip install -e .[test]
	npm install
	$(MANAGEPY) compilemessages -l de

.PHONY: migrate
migrate:
	$(MANAGEPY) migrate

.PHONY: populate
populate:
	$(MANAGEPY) shell -c "from django.contrib.auth.models import User; User.objects.filter(username='admin').exists() or User.objects.create_superuser('admin', 'admin@example.com', 'password')"

.PHONY: lint
lint:
	$(VIRTUAL_ENV)/bin/ruff check reimbursements

.PHONY: makemessages
makemessages:
	$(MANAGEPY) makemessages -l de -d django
