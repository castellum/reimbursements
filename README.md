This is prototype for a reimbursement tool for internal use at MPIB.

# Quickstart

Run `make` to start a development setup. python and npm are required.

For members of MPIB, a demo server is available at
<https://reimbursements.t.mpib-berlin.mpg.de>.

# Goals

-   Allow participants to enter their bank details
-   Allow the administration to get a weekly export of all requests
-   Allow study members to see the status of each request
-   Require verification from 3 different people to avoid abuse
    -   The participant
    -   The conductor
    -   The project leader
-   Provide some redundancy in the data so it can be checked for
    consistency
-   Prevent manipulation of the data once it has been entered
-   Allow a buffer between export and deletion of the exported data to
    make sure that everything worked as expected

# Non-Goals

-   This is not meant to cover all possible edge cases, but streamline
    the common case.
-   The participants' details are not meant to be stored for a long
    time. They should be processed and then deleted.
-   Reimbursement requests are not meant to be changed. If there is an
    issue, a new request should be created instead.

# Potential TODOs

-   Change the name. The software is called "reimbursements", but in the
    UI we use "expense allowance". Neither is particularily pretty.
-   Allow delete or retract requests
-   Split into three separate apps
-   Rename "owner" to "project manager"
