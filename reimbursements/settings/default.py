from pathlib import Path

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_bootstrap5',
    'bootstrap_colors',
    'reimbursements.main',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'reimbursements.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'reimbursements.wsgi.application'


# Password validation
# https://docs.djangoproject.com/en/4.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = '/'
LOGIN_URL = '/login/'

# Internationalization
# https://docs.djangoproject.com/en/4.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_TZ = True

LOCALE_PATHS = [
    BASE_DIR / 'locale',
]


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/4.2/howto/static-files/

STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'reimbursements.main.finders.NpmFinder',
]

STORAGES = {
    'default': {
        'BACKEND': 'django.core.files.storage.FileSystemStorage',
    },
    'staticfiles': {
        'BACKEND': 'django.contrib.staticfiles.storage.ManifestStaticFilesStorage',
    },
}

STATIC_URL = 'static/'

STATIC_ROOT = BASE_DIR.parent / 'static'

NPM_PATH = BASE_DIR.parent / 'node_modules'

NPM_FILES = [
    Path('bootstrap', 'dist', 'css', 'bootstrap.min.css'),
    Path('bootstrap', 'dist', 'css', 'bootstrap.min.css.map'),
    Path('@fortawesome', 'fontawesome-free', 'css', 'all.min.css'),
    Path('@fortawesome', 'fontawesome-free', 'webfonts', 'fa-regular-400.woff2'),
    Path('@fortawesome', 'fontawesome-free', 'webfonts', 'fa-regular-400.ttf'),
    Path('@fortawesome', 'fontawesome-free', 'webfonts', 'fa-brands-400.woff2'),
    Path('@fortawesome', 'fontawesome-free', 'webfonts', 'fa-brands-400.ttf'),
    Path('@fortawesome', 'fontawesome-free', 'webfonts', 'fa-solid-900.woff2'),
    Path('@fortawesome', 'fontawesome-free', 'webfonts', 'fa-solid-900.ttf'),
    Path('@fortawesome', 'fontawesome-free', 'webfonts', 'fa-v4compatibility.woff2'),
    Path('@fortawesome', 'fontawesome-free', 'webfonts', 'fa-v4compatibility.ttf'),
    Path('select', 'select.js'),
    Path('select', 'utils.js'),
    Path('select', 'values.js'),
    Path('select', 'select.css'),
]

BOOTSTRAP5 = {
    'set_placeholder': False,
}

BOOTSTRAP_THEME_COLORS = ['#0d6efd', '#0b5ed7', '#0a58ca']

# Default primary key field type
# https://docs.djangoproject.com/en/4.2/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'
