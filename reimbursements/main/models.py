import uuid

from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _


class Study(models.Model):
    uuid = models.UUIDField(_('UUID'), default=uuid.uuid4, primary_key=True)
    name = models.CharField(_('Name'), max_length=64)
    url = models.URLField(_('URL'))
    cost_center = models.CharField(_('Cost center'), max_length=32)
    owners = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        verbose_name=_('Owners'),
        related_name='+',
    )
    conductors = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        verbose_name=_('Conductors'),
        related_name='+',
    )

    def __str__(self):
        return self.name


class Export(models.Model):
    created_at = models.DateTimeField(_('Created at'), auto_now_add=True)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_('Created by'), on_delete=models.PROTECT
    )

    def __str__(self):
        return self.created_at.strftime('%Y-%m-%d_%H:%M:%S')


class Reimbursement(models.Model):
    created_at = models.DateTimeField(_('Created at'), auto_now_add=True)
    approved_by_conductor = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_('Approved by conductor'),
        blank=True,
        null=True,
        on_delete=models.PROTECT,
        related_name='+',
    )
    approved_by_owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_('Approved by owner'),
        blank=True,
        null=True,
        on_delete=models.PROTECT,
        related_name='+',
    )
    study = models.ForeignKey(Study, verbose_name=_('Study'), on_delete=models.PROTECT)
    export = models.ForeignKey(
        Export,
        verbose_name=_('Export'),
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )

    first_name = models.CharField(_('First name'), max_length=64)
    last_name = models.CharField(_('Last name'), max_length=64)

    street = models.CharField(_('Street'), max_length=64)
    house_number = models.CharField(_('House number'), max_length=16)
    zip_code = models.CharField(_('ZIP code'), max_length=16)
    city = models.CharField(_('City'), max_length=64)

    amount = models.DecimalField(_('Amount'), max_digits=10, decimal_places=2)
    account_holder = models.CharField(
        _('Account holder (if different)'), max_length=128, blank=True
    )
    iban = models.CharField(_('IBAN'), max_length=34)

    def get_account_holder(self):
        return self.account_holder or f'{self.first_name} {self.last_name}'
