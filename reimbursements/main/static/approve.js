document.addEventListener('submit', event => {
    event.preventDefault();
    var form = event.target;

    fetch(form.action, {
        method: form.method,
        body: new FormData(form),
    }).then(r => {
        if (r.ok) {
            var table = form.closest('table');
            form.parentElement.textContent = table.dataset.username;
        }
    });
});
