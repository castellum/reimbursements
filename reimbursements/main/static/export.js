document.addEventListener('submit', event => {
    event.preventDefault();
    var form = event.target;

    fetch(form.action, {
        method: form.method,
        body: new FormData(form),
    }).then(r => {
        if (r.ok) {
            var a = document.createElement('a');
            a.download = '';
            a.href = r.url;
            a.click();

            location.reload();
        }
    });
});
