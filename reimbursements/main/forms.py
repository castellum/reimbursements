from django import forms
from localflavor.generic.forms import IBANFormField

from .models import Reimbursement
from .models import Study


class ReimbursementForm(forms.ModelForm):
    class Meta:
        model = Reimbursement
        fields = [
            'first_name',
            'last_name',
            'street',
            'house_number',
            'zip_code',
            'city',
            'amount',
            'account_holder',
            'iban',
        ]
        field_classes = {
            'iban': IBANFormField,
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field in self.fields.values():
            field.widget.attrs['autocomplete'] = 'off'


class StudyForm(forms.ModelForm):
    class Meta:
        model = Study
        fields = ['name', 'url', 'cost_center', 'owners', 'conductors']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for key in ['owners', 'conductors']:
            self.fields[key].empty_label = ''
            self.fields[key].widget.attrs.update({
                'data-select': True,
                'data-select-input-class': 'form-control',
                'data-select-value-class': 'badge text-bg-secondary badge-delete',
            })
