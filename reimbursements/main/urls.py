from django.urls import path
from django.views.generic import TemplateView

from . import views

urlpatterns = [
    path('', views.StudyListView.as_view(), name='study-list'),
    path('create/', views.StudyCreateView.as_view(), name='study-create'),
    path('<uuid:pk>/', views.StudyDetailView.as_view(), name='study-detail'),
    path('<uuid:pk>/items/', views.StudyItemsView.as_view(), name='study-items'),
    path('<uuid:pk>/update/', views.StudyUpdateView.as_view(), name='study-update'),
    path('<uuid:pk>/delete/', views.StudyDeleteView.as_view(), name='study-delete'),
    path(
        '<uuid:study_pk>/create/',
        views.ReimbursementCreateView.as_view(),
        name='create',
    ),
    path(
        '<uuid:study_pk>/success/',
        TemplateView.as_view(template_name='main/success.html'),
        name='create-success',
    ),
    path(
        '<uuid:study_pk>/conductor_approve/<int:pk>/',
        views.ReimbursementConductorApproveView.as_view(),
        name='conductor-approve',
    ),
    path(
        '<uuid:study_pk>/owner_approve/<int:pk>/',
        views.ReimbursementOwnerApproveView.as_view(),
        name='owner-approve',
    ),
    path(
        '<uuid:study_pk>/delete/<int:pk>/',
        views.ReimbursementDeleteView.as_view(),
        name='reimbursement-delete',
    ),
    path('exports/', views.ExportListView.as_view(), name='export-list'),
    path('exports/create/', views.ExportCreateView.as_view(), name='export-create'),
    path('exports/<int:pk>/', views.ExportView.as_view(), name='export'),
    path(
        'exports/<int:pk>/delete/',
        views.ExportDeleteView.as_view(),
        name='export-delete',
    ),
]
