import csv

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import SuspiciousOperation
from django.db import models
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.urls import reverse
from django.views.generic import CreateView
from django.views.generic import DeleteView
from django.views.generic import DetailView
from django.views.generic import ListView
from django.views.generic import UpdateView
from django.views.generic import View

from .forms import ReimbursementForm
from .forms import StudyForm
from .mixins import ConductorRequiredMixin
from .mixins import MemberRequiredMixin
from .mixins import OwnerRequiredMixin
from .mixins import StudyMixin
from .models import Export
from .models import Reimbursement
from .models import Study


class StudyListView(LoginRequiredMixin, ListView):
    model = Study

    def get_queryset(self):
        return super().get_queryset().filter(
            models.Q(owners=self.request.user.pk)
            | models.Q(conductors=self.request.user.pk)
        ).distinct()


class StudyCreateView(PermissionRequiredMixin, CreateView):
    permission_required = ['main.add_study']
    model = Study
    form_class = StudyForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['initial'] = {
            'owners': [self.request.user],
        }
        return kwargs

    def get_success_url(self):
        return reverse('study-detail', args=[self.object.pk])


class StudyUpdateView(OwnerRequiredMixin, StudyMixin, UpdateView):
    model = Study
    form_class = StudyForm

    def get_success_url(self):
        return reverse('study-detail', args=[self.object.pk])


class StudyDeleteView(OwnerRequiredMixin, StudyMixin, DeleteView):
    model = Study

    def get_success_url(self):
        return reverse('study-list')


class StudyDetailView(MemberRequiredMixin, StudyMixin, DetailView):
    model = Study

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['is_owner'] = (
            self.request.user.is_superuser
            or self.object.owners.filter(pk=self.request.user.pk).exists()
        )
        context['is_conductor'] = (
            self.request.user.is_superuser
            or self.object.conductors.filter(pk=self.request.user.pk).exists()
        )
        context['create_url'] = self.request.build_absolute_uri(
            reverse('create', args=[self.object.pk])
        )
        return context


class StudyItemsView(MemberRequiredMixin, StudyMixin, DetailView):
    model = Study
    template_name = 'main/study_items.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['reimbursements'] = (
            self.object.reimbursement_set
            .select_related('approved_by_conductor')
            .select_related('approved_by_owner')
            .select_related('export')
            .order_by('-created_at')
        )
        context['is_owner'] = (
            self.request.user.is_superuser
            or self.object.owners.filter(pk=self.request.user.pk).exists()
        )
        context['is_conductor'] = (
            self.request.user.is_superuser
            or self.object.conductors.filter(pk=self.request.user.pk).exists()
        )
        context['create_url'] = self.request.build_absolute_uri(
            reverse('create', args=[self.object.pk])
        )
        return context


class ReimbursementCreateView(StudyMixin, CreateView):
    model = Reimbursement
    form_class = ReimbursementForm

    def get_success_url(self):
        return reverse('create-success', args=[self.kwargs['study_pk']])

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.study = self.study
        return super().form_valid(form)


class ReimbursementDeleteView(OwnerRequiredMixin, StudyMixin, DeleteView):
    model = Reimbursement

    def get_object(self):
        return get_object_or_404(
            Reimbursement, study=self.study, pk=self.kwargs['pk'], export=None
        )

    def get_success_url(self):
        return reverse('study-items', args=[self.study.pk])


class ReimbursementConductorApproveView(ConductorRequiredMixin, StudyMixin, View):
    def post(self, request, study_pk, pk):
        reimbursement = get_object_or_404(Reimbursement, study=self.study, pk=pk)

        reimbursement.approved_by_conductor = request.user
        reimbursement.save()

        return redirect('study-items', self.study.pk)


class ReimbursementOwnerApproveView(OwnerRequiredMixin, StudyMixin, View):
    def post(self, request, study_pk, pk):
        reimbursement = get_object_or_404(Reimbursement, study=self.study, pk=pk)

        if not reimbursement.approved_by_conductor:
            raise SuspiciousOperation('not approved by conductor')
        if reimbursement.approved_by_conductor == request.user:
            raise SuspiciousOperation('same user as conductor')

        reimbursement.approved_by_owner = request.user
        reimbursement.save()

        return redirect('study-items', self.study.pk)


class ExportListView(PermissionRequiredMixin, ListView):
    permission_required = ['main.add_export']
    model = Export

    def get_queryset(self):
        return (
            super().get_queryset()
            .annotate(count=models.Count('reimbursement'))
            .order_by('-created_at')
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['count'] = (
            Reimbursement.objects
            .exclude(approved_by_conductor=None)
            .exclude(approved_by_owner=None)
            .filter(export=None)
            .count()
        )
        return context


class ExportCreateView(PermissionRequiredMixin, View):
    permission_required = ['main.add_export']
    model = Export

    def post(self, request):
        export = Export.objects.create(created_by=request.user)
        reimbursements = (
            Reimbursement.objects
            .exclude(approved_by_conductor=None)
            .exclude(approved_by_owner=None)
            .filter(export=None)
        )
        reimbursements.update(export=export)
        return redirect('export', export.pk)


class ExportView(PermissionRequiredMixin, View):
    permission_required = ['main.add_export']
    model = Export

    def get(self, request, pk):
        export = get_object_or_404(Export, pk=pk)

        filename = f'reimbursements_{export}.csv'
        response = HttpResponse(content_type='text/csv', headers={
            'Content-Disposition': f'attachment; filename="{filename}"'
        })

        w = csv.writer(response)
        w.writerow([
            'study_name',
            'study_url',
            'cost_center',
            'created_at',
            'approved_by_conductor',
            'approved_by_owner',
            'first_name',
            'last_name',
            'street',
            'house_number',
            'zip_code',
            'city',
            'amount',
            'account_holder',
            'iban',
        ])
        for reimbursement in (
            export.reimbursement_set.order_by('created_at').select_related('study')
        ):
            w.writerow([
                reimbursement.study.name,
                reimbursement.study.url,
                reimbursement.study.cost_center,
                reimbursement.created_at,
                reimbursement.approved_by_conductor.username,
                reimbursement.approved_by_owner.username,
                reimbursement.first_name,
                reimbursement.last_name,
                reimbursement.street,
                reimbursement.house_number,
                reimbursement.zip_code,
                reimbursement.city,
                reimbursement.amount,
                reimbursement.get_account_holder(),
                reimbursement.iban,
            ])

        return response


class ExportDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = ['main.add_export']
    model = Export

    def get_success_url(self):
        return reverse('export-list')
