# Generated by Django 4.2.9 on 2024-01-12 11:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("main", "0004_alter_reimbursement_account_holder"),
    ]

    operations = [
        migrations.AddField(
            model_name="reimbursement",
            name="city",
            field=models.CharField(
                default="Berlin", max_length=64, verbose_name="City"
            ),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="reimbursement",
            name="house_number",
            field=models.CharField(
                default="0", max_length=16, verbose_name="House number"
            ),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="reimbursement",
            name="street",
            field=models.CharField(
                default="TODO", max_length=64, verbose_name="Street"
            ),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="reimbursement",
            name="zip_code",
            field=models.CharField(
                default="12345", max_length=16, verbose_name="ZIP code"
            ),
            preserve_default=False,
        ),
    ]
