from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("main", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="reimbursement",
            name="iban",
            field=models.CharField(max_length=34, verbose_name="IBAN"),
        ),
    ]
