from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("main", "0002_alter_reimbursement_iban"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="reimbursement",
            name="bank",
        ),
        migrations.RemoveField(
            model_name="reimbursement",
            name="bic",
        ),
        migrations.RemoveField(
            model_name="reimbursement",
            name="city",
        ),
        migrations.RemoveField(
            model_name="reimbursement",
            name="house_number",
        ),
        migrations.RemoveField(
            model_name="reimbursement",
            name="street",
        ),
        migrations.RemoveField(
            model_name="reimbursement",
            name="zip_code",
        ),
    ]
