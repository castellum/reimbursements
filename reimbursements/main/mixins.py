from django.contrib.auth.mixins import AccessMixin
from django.shortcuts import get_object_or_404
from django.utils.functional import cached_property

from .models import Study


class StudyMixin:
    @cached_property
    def study(self):
        if getattr(self, 'model', None) is Study:
            if getattr(self, 'object', None):
                return self.object
            else:
                return self.get_object()
        else:
            return get_object_or_404(Study, pk=self.kwargs['study_pk'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['study'] = self.study
        return context


class ConductorRequiredMixin(AccessMixin):
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated or (
            not request.user.is_superuser
            and not self.study.conductors.filter(pk=request.user.pk).exists()
        ):
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)


class OwnerRequiredMixin(AccessMixin):
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated or (
            not request.user.is_superuser
            and not self.study.owners.filter(pk=request.user.pk).exists()
        ):
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)


class MemberRequiredMixin(AccessMixin):
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated or (
            not request.user.is_superuser
            and not self.study.conductors.filter(pk=request.user.pk).exists()
            and not self.study.owners.filter(pk=request.user.pk).exists()
        ):
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)
