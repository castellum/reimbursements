from django.contrib import admin

from . import models

admin.site.register(models.Study)
admin.site.register(models.Export)
admin.site.register(models.Reimbursement)
