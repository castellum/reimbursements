from bootstrap_colors.views import BootstrapColorsView
from django.contrib import admin
from django.contrib.auth.views import LoginView
from django.contrib.auth.views import LogoutView
from django.urls import include
from django.urls import path
from django.views.decorators.cache import cache_control

urlpatterns = [
    path('login/', LoginView.as_view(template_name='login.html'), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('admin/', admin.site.urls),
    path('colors.css', cache_control(max_age=86400)(
        BootstrapColorsView.as_view()
    ), name='colors'),
    path('', include('reimbursements.main.urls')),
]
